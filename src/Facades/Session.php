<?php namespace Tekton\Session\Facades;

class Session extends \Tekton\Support\Facade {
    protected static function getFacadeAccessor() { return 'session'; }
}
